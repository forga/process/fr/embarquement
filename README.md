Avertissement
-------------

Ceci est un modèle de projet ayant pour but de faciliter l'accompagnement de nouveaux collaborateur·ice·s dans une équipe en utilisant les fonctionnalités de [gestion de projet de _GitLab_](https://about.gitlab.com/stages-devops-lifecycle/plan/). Il à été imaginé dans un contexte de TPE pour du developpement utilisant `python` & `django` mais devrait pouvoir s'adapter à de nombreux autres contextes.

Le projet original est disponible à cette URL : [`https://gitlab.com/forga/process/fr/embarquement/`](https://gitlab.com/forga/process/fr/embarquement/)  sous les termes de la [_GNU Free Documentation License_](https://gitlab.com/forga/process/fr/embarquement/-/blob/production/LICENSE.md), toute contribution est bienvenue!


Embarquement
============

L'embarquement est facultatif, il vise à s'assurer que l'on part sur une base commune.
Il commence avec l'ouverture d'un ticket correspondant à un profil.
On y coche les tâches au fur et à mesure de leur réalisation :
* [Étudiant·e DAPy](https://gitlab.com/forga/process/fr/embarquement/-/issues/new?issue[title]=Embarquement%20de%20<mon%20nom>&issuable_template=profil-oc-dapy)
* [Dev Python](https://gitlab.com/forga/process/fr/embarquement/-/issues/new?issue[title]=Embarquement%20de%20<mon%20nom>&issuable_template=profil-devel-python)


Parcours de tutorat
===================

Un tuto démarre avec l'ouverture d'un nouveau ticket :
1. «[_Un script durable en Python_](https://gitlab.com/forga/process/fr/embarquement/-/issues/new?issue[title]=Tuto%20PySDur%20de%20<mon%20nom>&issuable_template=tutorat-python-script-durable)»
1. «[_GitLab_ & _Django_](https://gitlab.com/forga/process/fr/embarquement/-/issues/new?issue[title]=Tuto%20GitLab-Django%20de%20<mon%20nom>&issuable_template=tutorat-gitlab-django)»
1. «[_PlantUML_](https://gitlab.com/forga/process/fr/embarquement/-/issues/new?issue[title]=Tuto%20PlantUML%20de%20<mon%20nom>&issuable_template=tutorat-puml)»
1. «[_Pytest_/_GitLab CI_](https://gitlab.com/forga/process/fr/embarquement/-/issues/new?issue[title]=Tuto%20Pytest-Gitlab%20CI%20de%20<mon%20nom>&issuable_template=tutorat-pytest-gitlab_ci)»


Votre avis compte!
==================

Ne pas hésiter à [proposer une mise à jour](https://gitlab.com/-/ide/project/forga/process/fr/embarquement/edit/production/-/.gitlab/issue_templates) des modèles!
