Embarquement OC-DAPy
====================

* GitLab
* [ ]  [se connecter](https://gitlab.com/users/login) à mon compte ou [en créer un](https://gitlab.com/users/login)
* [ ]  [ajouter sa clé ssh](https://gitlab.com/profile/keys) à mon compte
*  installation des outils de développement
    * [ ]  `<mon éditeur préféré> `
    * [ ]  [`git`](https://git-scm.com/book/fr/v2/D%C3%A9marrage-rapide-Installation-de-Git)
    * [ ]  [`VirtualBox`](https://www.virtualbox.org/manual/UserManual.html#installation)
        * [ ]  une VM [`Debian/stable`](https://www.debian.org/releases/stable/) minimaliste
    * [ ]  `python3` : la [version identique à celle fournie par `Debian/stable`](https://packages.debian.org/stable/python3)
        * `Debian` : suivre [cette méthode](https://unix.stackexchange.com/a/350985) pour une installation _multi-version_ de python en _espace utilisateur_
        *  [`pyenv`](https://github.com/pyenv/pyenv#installation) peut-être une option aussi
    * [ ]  [`virtualenv`](https://virtualenv.pypa.io/en/latest/installation.html)
    * [ ]  [`black`](https://github.com/psf/black#installation-and-usage)
    * [ ]  [`flake8`](http://flake8.pycqa.org/en/latest/index.html#installation)
    * [ ]  [`pylint`](http://pylint.pycqa.org/en/latest/user_guide/installation.html)
    * [ ]  [`pytest`](https://docs.pytest.org/en/latest/getting-started.html)
* [ ]  Lire [la convention de contribution `git`](https://forga.gitlab.io/process/fr/manuel/convention/git/)
* [ ]  proposer une amélioration (au moins) du [modèle de cette liste](https://gitlab.com/forga/process/fr/embarquement/edit/stable/.gitlab/issue_templates/profil-oc-dapy.md)

L'embarquement terminé, la suite est là:

* [ ]  Tuto «[_Un script durable en Python_](https://gitlab.com/forga/process/fr/embarquement/-/issues/new?issue[title]=Tuto%20PySDur%20de%20<mon%20nom>&issuable_template=tutorat-python-script-durable)»
* [ ]  Tuto «[_GitLab/Django_](https://gitlab.com/forga/process/fr/embarquement/-/issues/new?issue%5Btitle%5D=Tutorat%20GitLab-Django%20de%20<mon%20nom>&issuable_template=tutorat-gitlab-django)»
* [ ]  Tuto «[_PlantUML_](https://gitlab.com/forga/process/fr/embarquement/-/issues/new?issue%5Btitle%5D=Tutorat%20PlantUML%20de%20<mon%20nom>&issuable_template=tutorat-puml)»
* [ ]  Tuto «[_Pytest/Gitlab CI_](https://gitlab.com/forga/process/fr/embarquement/-/issues/new?issue%5Btitle%5D=Tutorat%20Pytest-GitLab%20CI%20de%20<mon%20nom>&issuable_template=tutorat-pytest-gitlab_ci)»


<!-- Quick actions GitLab : ne rien écrire d'autre sous cette ligne SVP  -->
/cc @all
/assign me
/due in 1 day
/todo
/label ~"follow::todo"
