Prérequis
---------

Aucun !

À faire
-------

* [ ]  Faire le [tuto Django «Polls»](https://docs.djangoproject.com/fr/stable/intro/tutorial01/) dans un _nouveau dépôt personnel_ : `https://gitlab.com/<user>/mon_tuto_django`
    * [ ]  mettre le lien ici : [`<user>/mon_tuto_django`](https://gitlab.com/<user>/mon_tuto_django)
    * [ ]  mentionner _cette issue_ dans le commit initial de `<user>/mon_tuto_django` en utilisant le [référencement de commit](https://docs.gitlab.com/ce/user/project/issues/managing_issues.html#default-closing-pattern) (voir aussi «[_autocomplete_characters_](https://docs.gitlab.com/ce/user/project/autocomplete_characters.html)»)
*  Déployer localement ces projets Django :
    * [ ]  [`tool/django/core`](https://gitlab.com/forga/tool/django/core/)
    * [ ]  [`client/ACME`](https://gitlab.com/forga/customer/acme)
*  Contribuer aux projets ci dessous avec une [demande de fusion](https://docs.gitlab.com/ce/user/project/merge_requests/index.html) (_merge request_) qui met à jour Django avec la [dernière version stable](https://www.djangoproject.com/download/) :
    * [ ]  [`client/ACME`](https://gitlab.com/forga/customer/acme)
    * [ ]  faire remonter les mises à jour vers le dépôt amont de [`client/ACME`] : [`tool/django/core`](https://gitlab.com/forga/tool/django/core/)
* [ ]  proposer une amélioration (au moins) du [modèle de ce ticket (_issue_)](https://gitlab.com/forga/process/fr/embarquement/-/edit/production/.gitlab/issue_templates/tutorat-gitlab-django.md)


Suite
-----

Ce ticket (_issue_) terminé, passer à la découverte de la génération de diagrammes avec le tutorat «[_PlantUML_](https://gitlab.com/forga/process/fr/embarquement/-/issues/new?issue[title]=Tutorat%20PlantUML%20de%20<mon%20nom>&issuable_template=tutorat-puml)».


<!-- Quick actions GitLab : ne rien écrire d'autre sous cette ligne SVP  -->
ping @forga/devel
/assign me
/due in 2 week
/todo
/label ~"follow::todo"
