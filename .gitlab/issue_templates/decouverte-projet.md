<!-- Si on à les droits sur le projet à faire découvrir, ce modèle de ticket est plutôt à ajouter au projet dans le répertoire : `.gitlab/issue_templates/decouverte-<nom_projet>.md` -->

Prise en main du projet [`<chemin>/<du>/<projet>`](https://gitlab.com/<chemin>/<du>/<projet>/) (_<nom_du_projet>_):

* [ ]  Lire le [`README.md`](https://gitlab.com/<chemin>/<du>/<projet>/-/blob/master/README.md)
* [ ]  Regarder le [tableau (_board_))](https://gitlab.com/<chemin>/<du>/<projet>/-/boards/) de suivi
* [ ]  Déployer le projet localement :
    - [ ] poser les [questions](https://gitlab.com/<chemin>/<du>/<projet>/-/issues/new) et faire les [commentaires](https://gitlab.com/<chemin>/<du>/<projet>/-/issues/new) nécessaire si il manque des informations pour un déploiement fonctionnel
* [ ]  Générer en local les diagrammes (dans la branche [`documentation`](https://gitlab.com/<chemin>/<du>/<projet>/tree/documentation))
* [ ]  Regarder les [jalons à livrer (_milestones_)](https://gitlab.com/<chemin>/<du>/<projet>/milestones) (lots de tickets)
* [ ]  Choisir un [ticket (_issue_)](https://gitlab.com/<chemin>/<du>/<projet>/-/issues/) que tu te sens capable de résoudre
* [ ]  Proposer au moins une amélioration du [modèle de cette _TODOlist_](https://gitlab.com/forga/process/fr/embarquement/-/edit/production/.gitlab/issue_templates/decouverte-projet.md)


<!-- Quick actions GitLab : ne rien écrire d'autre sous cette ligne SVP  -->
/cc @all
/assign me
/due in 2 day
/todo
/label ~"follow::todo"
