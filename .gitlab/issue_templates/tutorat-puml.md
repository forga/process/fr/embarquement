### Prérequis

* Avoir déjà fait le tutorat «[_GitLab/Django_](https://gitlab.com/forga/process/fr/embarquement/-/issues/new?issue%5Btitle%5D=Tutorat%20GitLab-Django&issuable_template=tutorat-gitlab-django)»


### À faire

* [ ]  [Installer PlantUML](https://plantuml.com/fr/starting) localement ou [l'intégrer à son EDI](https://plantuml.com/fr/running) pour développer plus facilement
* Générer localement les diagrammes avec les sources des exemples de `Modèle PlantUML` :
    * en _fichier seul_ :
        * [ ]  modèle fonctionnel détaillé : [sources du diagramme][fmwa-single-src]
        * [ ]  modèle physique de donnée : [sources du diagramme][pdm-single-src]
    * en _fichiers partagés_ :
        * [ ]  modèle fonctionnel : [sources du diagramme][fm-shared-src] ([fichiers partagés][part])
        * [ ]  modèle fonctionnel détaillé : [sources du diagramme][fmwa-shared-src]  ([fichiers partagés][part])
        * [ ]  modèle physique de donnée : [sources du diagramme][pdm-shared-src]  ([fichiers partagés][part])
    * ajouter une classe `acme` avec 3 attributs (au choix) aux diagrammes ci-dessus (fichiers partagés). Cette classe sera en relation avec la classe `foo`. Modifier la légende du diagramme pour y mettre cette consigne. Une fois la nouvelle image générée, la poster dans les commentaires (après avoir cocher la tâche) :
        * [ ]  modèle fonctionnel
        * [ ]  modèle fonctionnel détaillé
        * [ ]  modèle physique de donnée
    * modifier la disposition des classes (mettre `foo` à droite et `bar` en haut de `user`). Modifier la légende du diagramme pour y mettre cette consigne. Une fois la nouvelle image générée, la poster dans les commentaires (après avoir cocher la tâche) :
        * [ ]  modèle fonctionnel
        * [ ]  modèle fonctionnel détaillé
        * [ ]  modèle physique de donnée
* Contribuer au dépôt [`Modèle PlantUML`](https://gitlab.com/forga/devel/third-party/mypumlt) avec :
    * [ ]  le diagramme _Modèle physique de donnée_ avec la classe `acme` et la disposition modifié
    * [ ]  un nouveau diagramme inspiré du modèle de donné utilisé dans `<user>/mon_tuto_django`
* [ ]  Proposer une amélioration (au moins) du [modèle de ce tutorat](https://gitlab.com/forga/process/fr/embarquement/-/edit/production/.gitlab/issue_templates/tutorat-puml.md)

Ce tuto terminé, passer à la découverte de l'intégration continue _GitLab CI_ avec le tutorat «[_Pytest_/_GitLab CI_](https://gitlab.com/forga/process/fr/embarquement/-/issues/new?issue[title]=Tutorat%20Pytest-Gitlab%20CI%20de%20<mon%20nom>&issuable_template=tutorat-pytest-gitlab_ci)» pour générer automatiquement les diagrammes (mais pas seulement).


[fm-shared-src]:    https://gitlab.com/forga/devel/third-party/mypumlt/raw/master/puml/fm-shared_files.puml "Diagram sources"
[fmwa-single-src]:  https://gitlab.com/forga/devel/third-party/mypumlt/raw/master/puml/fmwa-single_file.puml "Diagram sources"
[fmwa-shared-src]:  https://gitlab.com/forga/devel/third-party/mypumlt/raw/master/puml/fmwa-shared_files.puml "Diagram sources"
[part]:             https://gitlab.com/forga/devel/third-party/mypumlt/blob/master/puml/part "Diagram parts sources"
[pdm-shared-src]:   https://gitlab.com/forga/devel/third-party/mypumlt/raw/master/puml/pdm-shared_files.puml "Diagram sources"
[pdm-single-src]:   https://gitlab.com/forga/devel/third-party/mypumlt/raw/master/puml/pdm-single_file.puml "Diagram sources"


<!-- Quick actions GitLab : ne rien écrire d'autre sous cette ligne SVP  -->
/cc @all
/assign me
/due in 1 week
/todo
/label ~"follow::todo"
