### Prérequis

* Avoir déja fait le tutorat «[_GitLab/Django_](https://gitlab.com/forga/process/fr/embarquement/-/issues/new?issue%5Btitle%5D=Tutorat%20GitLab-Django&issuable_template=tutorat-gitlab-django)»
* Avoir déja fait le tutorat «[_PlantUML_](https://gitlab.com/forga/process/fr/embarquement/-/issues/new?issue%5Btitle%5D=Tutorat%20PlantUML&issuable_template=tutorat-puml)»

### À faire

* Faire les tutoriels ci-dessous dans un _nouveau dépôt personnel_. S'arrêter avant le chapitre traitant de l'implémentation dans _Semaphore CI/CD_ :
    - [ ]  «[_Testing Python Applications with Pytest_](https://semaphoreci.com/community/tutorials/testing-python-applications-with-pytest)»
        * [ ]  référencer _cette issue_ dans les commits du tuto
        * [ ]  mettre le lien ici : [`<user>/mon_tuto_pytest`](https://gitlab.com/<user>/mon_tuto_pytest)
    * [ ]  «[_Building and Testing an API Wrapper in Python_](https://semaphoreci.com/community/tutorials/building-and-testing-an-api-wrapper-in-python)»
        * [ ]  référencer _cette issue_ dans les commits du tuto
        * [ ]  mettre le lien ici : [`<user>/mon_tuto_api_wrapper`](https://gitlab.com/<user>/mon_tuto_api_wrapper)
        * [ ]  mettre en marque-page ce tutoriel pour son approche du [TDD](https://fr.wikipedia.org/wiki/Test_driven_development)
* [ ]  Utiliser [`pytest-django`](https://pytest-django.readthedocs.io/en/latest/) : en l'ajouter aux dépendances de `<user>/mon_tuto_django/` (mettre à jour le lien dans cette _issue_)
* [ ]  Utiliser `pytest` dans `polls.tests` et convertir les tests existants
* [ ]  Lire «[_Getting started with GitLab CI/CD_](https://docs.gitlab.com/ce/ci/quick_start/README.html)». Le paragraphe traitant des _runners_ pourra être lu plus tard.
* [ ]  Mettre en marque-page «[_GitLab CI/CD Pipeline Configuration Reference_](https://docs.gitlab.com/ce/ci/yaml/README.html)» (pour plus tard) : c'est la documentation complète des fichiers `.gitlab-ci.yaml`
* [ ]  Regarder quelques configurations d'intégration continue _GitLab_  basiques :
    - [ ]  déploiement d'une page `html` sur _GitLab pages_ : [`pages/plain-html`](https://gitlab.com/pages/plain-html/-/blob/master/.gitlab-ci.yml)
    - [ ]  déploiement de plusieurs pages `html` générées par [`pelican`](https://blog.getpelican.com/) sur _GitLab pages_ : [`pages/pelican`](https://gitlab.com/pages/pelican/-/blob/master/.gitlab-ci.yml)
    - [ ]  construction de diagrammes avec `PlantUML` : [`third-party/mypumlt`](https://gitlab.com/forga/devel/third-party/mypumlt/blob/master/.gitlab-ci.yml)
    - [ ]  test et _lint_ d'un projet Django : [`tool/django/core`](https://gitlab.com/forga/tool/django/core/.gitlab-ci.yml)
    - [ ]  test avec plusieurs version de python  : [`localg-host/watchghost`](https://gitlab.com/localg-host/watchghost/-/blob/master/.gitlab-ci.yml)
* [ ]  Regarder quelques configurations d'intégration continue _GitLab_  plus élaboré :
    - [ ]  déploiement d'une application [flask](https://flask.palletsprojects.com/en/1.1.x/) avec [`zappa`](https://github.com/Miserlou/Zappa/) sur une infra _AWS_ : [`free_zed/grandpy`](https://gitlab.com/free_zed/grandpy/-/blob/master/.gitlab-ci.yml)
    - [ ]  test et déploiement d'une application Python sur [Heroku](https://heroku.com) : «[_GitLab CI/CD Examples_](https://docs.gitlab.com/ce/ci/examples/test-and-deploy-python-application-to-heroku.html)»
    - [ ]  un dernier exemple plus complexe, la configuration de [`www.gitlab.com`](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/.gitlab-ci.yml)
* [ ]  Mettre en place la CI en ajoutant un `.gitlab-ci.yml` a `<user>/mon_tuto_pytest`
* [ ]  Mettre en place la CI en ajoutant un `.gitlab-ci.yml` a `<user>/mon_tuto_api_wrapper`
* [ ]  Valider le fonctionnement de la CI en ajoutant les tests ci-dessous dans `<user>/mon_tuto_pytest` :
    - [ ]  tester les routes fournie par `mysite.urls`
    - [ ]  tester le retour d'une _erreur 404_ si une URL non prise en charge est appelée
    - [ ]  tester la présences des fichiers statiques (indice : [`django.contrib.staticfiles`](https://docs.djangoproject.com/fr/stable/ref/contrib/staticfiles/#findstatic))
    - [ ]  tester le bon chargement des fichiers statiques
    - [ ]  tester les méthodes de `polls/models`
    - [ ]  tester les méthodes de `polls/models`
* Retour sur _PlantUML_ : ajouter une branche `documentation` au dépôt `<user>/mon_tuto_django` :
    * [ ]  mettre en place la _CI GitLab_ pour générer les diagrammes (fais lors du _Tutoriel PlantUML_)
    * [ ]  mettre à jour le `README.md` et y intégrer les images du _MPD_ qui seront générée par _GitLab CI_
* [ ]  Proposer une amélioration (au moins) du [modèle de ce tutorat](https://gitlab.com/forga/process/fr/embarquement/-/edit/stable/.gitlab/issue_templates/tutorat-pytest-gitlab_ci.md)


---

* 🍻 Merci [Kevin Ndung'u Gathuku](https://kevgathuku.me/) pour ses tutos


<!-- Quick actions GitLab : ne rien écrire d'autre sous cette ligne SVP  -->
/assign me
/cc @all
/due in 1 week
/todo
/label ~"follow::todo"
